extern crate chrono;
extern crate serde;
extern crate serde_json;

#[derive(Serialize, Deserialize)]
pub struct TravelClaim {
    pub id: i32,
    pub clientname: String,
    pub beginkilos: f64,
  	pub total: f64 ,
  	pub date : String,
    pub user: Option<i32>
}