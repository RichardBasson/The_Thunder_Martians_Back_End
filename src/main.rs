#![feature(plugin)]
#![feature(custom_derive)]
#![feature(custom_attribute)]
#![plugin(rocket_codegen)]

#[macro_use]
extern crate serde_derive;

extern crate rocket;
extern crate rocket_contrib;
extern crate chrono;
extern crate serde;
extern crate serde_json;
extern crate postgres;

mod travelClaim;

use travelClaim::{TravelClaim};
use rocket_contrib::{Json, Value};
use postgres::{Connection, TlsMode};


#[get("/<name>/<age>")]
fn hello(name: String, age: u8) -> String {
    format!("Hello, {} year old named {}!", age, name)
}

#[post("/", data = "<travelclaim>")]
fn create_claim_entry(travelclaim: Json<TravelClaim>) -> Json<TravelClaim>{
                 travelclaim
}

fn main() {
  let conn = Connection::connect("postgresql://postgres:postgres@localhost:5432/postgres", TlsMode::None).unwrap();
   conn.execute("CREATE TABLE IF NOT EXISTS Users (
                   id              integer PRIMARY KEY,
                   Name            VARCHAR NOT NULL,
                   Admin           boolean
                 )", &[]).unwrap();

   conn.execute("CREATE TABLE IF NOT EXISTS TravelClaim (
                   id              integer PRIMARY KEY,
                   ClientName            VARCHAR NOT NULL,
                   BeginKilos            NUMERIC,
                   Total                   NUMERIC,
                   Date                    date,
                   Users                    integer REFERENCES Users
                 )", &[]).unwrap();

   if conn.is_active() {
       conn.finish();
   }

        rocket::ignite()
        .mount("/travelclaim", routes![create_claim_entry])
        .mount("/hello", routes![hello])
        .launch();
}
